import * as React from "react"
import Navbar from "../components/Navbar"
import styles from "../index.css"
import { Container, Grid, NextUIProvider } from "@nextui-org/react"
import { theme } from "../Theme"
import { StaticImage } from "gatsby-plugin-image"

const About = () => {
  return (
    <main style={styles}>
      <title>Sobre o site</title>
      {Navbar()}
      <NextUIProvider theme={theme}>
        <hr color={'white'} style={{ height: '1px', border: 'none' }} />
        <Container>
          <Grid.Container justify="center">
            <Grid justify="center">
              <h1>Mais sobre o site</h1>
            </Grid>

            <Grid>
              <p style={{textAlign: 'justify'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas magna dolor, cursus sed nunc sed, commodo egestas tortor. Mauris eleifend dolor ligula, eu hendrerit turpis sollicitudin eget. Maecenas eleifend nisl mi, dictum bibendum lorem tincidunt et. Aenean nisi libero, fringilla eget scelerisque sit amet, condimentum vel lacus. Fusce euismod libero mi, eget imperdiet dui vulputate non. Maecenas euismod euismod erat, id convallis turpis laoreet a. Quisque sagittis rhoncus lectus rhoncus malesuada. Nullam sit amet neque non ex molestie finibus sit amet tincidunt lorem. Etiam aliquet pulvinar nisl vel dignissim. Nulla nec mauris ut nibh dignissim eleifend in non felis. Donec nisl ligula, tempor vitae urna a, fermentum aliquet justo.

                Nam viverra vehicula nulla, at molestie ex consectetur imperdiet. Sed faucibus iaculis interdum. In nec luctus nulla, at sodales libero. Curabitur convallis justo vitae dui dapibus commodo. Suspendisse semper dolor nunc, eget vulputate nibh gravida ac. Nulla convallis, dolor quis placerat consequat, nulla lorem finibus odio, nec maximus augue quam id ipsum. Duis a leo et leo fermentum euismod. Duis quis lacus dignissim, finibus massa lobortis, consequat est. Nulla luctus eleifend mauris, sed fermentum ante scelerisque a. Donec ac porttitor nisl, vitae malesuada justo. Mauris dictum nibh in tellus iaculis, id ullamcorper sapien volutpat.

                Vivamus aliquet libero mauris, a pellentesque augue blandit feugiat. Cras vel suscipit purus. In imperdiet metus odio, sit amet pretium dolor laoreet ornare. Sed ac augue tempus, consequat libero vitae, luctus massa. Aenean ut viverra velit. Sed in convallis quam. Suspendisse rutrum tempus lacus sed consectetur. Cras semper porta purus sit amet gravida. Donec ac pharetra orci.</p>
            </Grid>
            
            <Grid>
            <StaticImage src={'../../images/big_bang.jpg'}  imgStyle={{borderRadius: '10px'}}
                           width={400} objectPosition={'center'}/>
            </Grid>

            <Grid>
              <p style={{textAlign: 'justify'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas magna dolor, cursus sed nunc sed, commodo egestas tortor. Mauris eleifend dolor ligula, eu hendrerit turpis sollicitudin eget. Maecenas eleifend nisl mi, dictum bibendum lorem tincidunt et. Aenean nisi libero, fringilla eget scelerisque sit amet, condimentum vel lacus. Fusce euismod libero mi, eget imperdiet dui vulputate non. Maecenas euismod euismod erat, id convallis turpis laoreet a. Quisque sagittis rhoncus lectus rhoncus malesuada. Nullam sit amet neque non ex molestie finibus sit amet tincidunt lorem. Etiam aliquet pulvinar nisl vel dignissim. Nulla nec mauris ut nibh dignissim eleifend in non felis. Donec nisl ligula, tempor vitae urna a, fermentum aliquet justo.

                Nam viverra vehicula nulla, at molestie ex consectetur imperdiet. Sed faucibus iaculis interdum. In nec luctus nulla, at sodales libero. Curabitur convallis justo vitae dui dapibus commodo. Suspendisse semper dolor nunc, eget vulputate nibh gravida ac. Nulla convallis, dolor quis placerat consequat, nulla lorem finibus odio, nec maximus augue quam id ipsum. Duis a leo et leo fermentum euismod. Duis quis lacus dignissim, finibus massa lobortis, consequat est. Nulla luctus eleifend mauris, sed fermentum ante scelerisque a. Donec ac porttitor nisl, vitae malesuada justo. Mauris dictum nibh in tellus iaculis, id ullamcorper sapien volutpat.

                Vivamus aliquet libero mauris, a pellentesque augue blandit feugiat. Cras vel suscipit purus. In imperdiet metus odio, sit amet pretium dolor laoreet ornare. Sed ac augue tempus, consequat libero vitae, luctus massa. Aenean ut viverra velit. Sed in convallis quam. Suspendisse rutrum tempus lacus sed consectetur. Cras semper porta purus sit amet gravida. Donec ac pharetra orci.</p>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default About


