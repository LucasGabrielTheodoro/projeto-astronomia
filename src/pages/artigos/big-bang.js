import * as React from "react"
import marina from '../../images/marina.png'
import {StaticImage} from "gatsby-plugin-image"
import Navbar from "../../components/Navbar"
import ArticleAuthor from "../../components/ArticleAuthor"
import styles from "../../index.css"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../../Theme"

const BigBang = () => {
  return (
    <main style={styles}>
      <title>Como surgiu o Big Bang</title>
      {Navbar()}
      <NextUIProvider theme={theme}>
        <hr color={'white'} style={{height: '1px', border: 'none'}}/>
        <div style={{marginTop: '55px'}}>
          <h1 style={{textDecoration: 'underline', textAlign: 'center', padding: '0 10px'}}>Como surgiu o Big Bang</h1>
        </div>
        {ArticleAuthor('Marina Diamandis', marina)}
        <Container>
          <Grid.Container gap={2} justify={'center'} alignItesm={'center'}>
            <Grid sm={12}>
              <p style={{textAlign: 'justify'}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit,
                mauris
                vel vestibulum aliquet, elit lacus scelerisque ante, ut venenatis est ante ut lorem. Duis fringilla
                accumsan
                diam in sagittis. Nam dignissim accumsan lectus vel posuere. Nulla fermentum nisl in erat ultrices
                tristique.
                Quisque at nisl id risus tempus condimentum vel et odio.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit, mauris vel vestibulum aliquet, elit
                lacus scelerisque ante, ut venenatis est ante ut lorem.
                In hac habitasse platea dictumst. Nam malesuada eros eget vulputate gravida. Maecenas erat est, maximus
                sit
                amet varius eget, eleifend eget mi. Nulla non lectus arcu. Nullam aliquam magna non nunc dictum, id
                convallis
                quam lacinia. Interdum et malesuada fames ac ante ipsum primis in faucibus. In auctor tristique sapien,
                porttitor elementum ipsum auctor id. Pellentesque faucibus interdum justo eget facilisis.</p>
            </Grid>
            <Grid md={6} lg={6} sm={12}>
              <p style={{textAlign: 'justify'}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit, mauris vel vestibulum aliquet, elit
                lacus scelerisque ante, ut venenatis est ante ut lorem. Duis fringilla accumsan diam in sagittis. Nam
                dignissim accumsan lectus vel posuere. Nulla fermentum nisl in erat ultrices tristique. Quisque at nisl id
                risus tempus condimentum vel et odio.
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam blandit, mauris vel vestibulum aliquet, elit
                lacus scelerisque ante, ut venenatis est ante ut lorem.
                In hac habitasse platea dictumst. Nam malesuada eros eget vulputate gravida. Maecenas erat est, maximus
                sit amet varius eget, eleifend eget mi. Nulla non lectus arcu. Nullam aliquam magna non nunc dictum, id
                convallis quam lacinia. Interdum et malesuada fames ac ante ipsum primis in faucibus. In auctor tristique
                sapien, porttitor elementum ipsum auctor id. Pellentesque faucibus interdum justo eget facilisis.
              </p>
            </Grid>
            <Grid md={6} lg={6} sm={12} justify={'center'} alignItems={'center'}>
              <StaticImage src={'../../images/big_bang.jpg'} alt={'big bang'} imgStyle={{borderRadius: '10px'}}
                           width={400} objectPosition={'right bottom'}/>
            </Grid>
          </Grid.Container>
        </Container>
      </NextUIProvider>
    </main>
  )
}

export default BigBang
