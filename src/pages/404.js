import * as React from "react"
import {Link} from "gatsby"
import style from "../index.css"
import {theme} from "../Theme"
import {NextUIProvider} from "@nextui-org/react"




// markup

const NotFoundPage = () => {
  return (
    <main style={style}>
      <title>Not found</title>
      <NextUIProvider theme={theme}>
      <div className = "centerPage">
        <h1>Page not found</h1>
        <p>
          Sorry{" "}
          <span role="img" aria-label="Pensive emoji">
            😔
          </span>{" "}
          we couldn’t find what you were looking for.
          <br />
          <Link to="/">Go home</Link>.
        </p>
      </div>
      </NextUIProvider>
    </main>
  )
}

export default NotFoundPage
