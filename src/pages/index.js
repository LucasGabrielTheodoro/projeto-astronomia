import * as React from "react"
import style from "../index.css"
import {StaticImage} from "gatsby-plugin-image"
import {Container, Grid, NextUIProvider} from "@nextui-org/react"
import {theme} from "../Theme"
import articles from "../articles.json"
import Article from "../components/Article"
import Navbar from "../components/Navbar"

const IndexPage = () => {
  return (
    <main style={style}>
      <title>Home Page</title>
      <NextUIProvider theme={theme}>
        <div style={{marginBottom: '11px'}}>
          <StaticImage src={'../images/banner.png'} alt={'banner'} placeholder={"blurred"} layout={'fullWidth'}/>
        </div>
        {Navbar()}
        <h2 style={{textAlign: 'center', fontSize: '38px', fontWeight: '500', marginBottom: '42px'}}>🌠 Início</h2>
        <div style={{marginBottom: '50px'}}>
          <Container>
            <Grid.Container gap={1.5} justify="center">
              {articles.articles.map(a => {
                return (
                  <Grid xs={12} md={4} lg={4} sm={6} justify="center">
                    {Article(a)}
                  </Grid>
                )
              })}
            </Grid.Container>
          </Container>
        </div>
      </NextUIProvider>

    </main>
  )
}

export default IndexPage
