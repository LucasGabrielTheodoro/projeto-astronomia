import React from 'react'
import {Card} from "@nextui-org/react"
import {navigate} from "gatsby"
import marina_lf from '../images/marina_lf.png'

const Article = (props) => {
  return (
    <Card clickable bordered css={{maxWidth: 400, maxHeight: 450}} onClick={() => { navigate(props.path) }}>
      <Card.Image
        src={marina_lf}
        alt={'card img bkg'} width={400} height={170} showSkeleton={false} objectFit={'cover'}/>
      <Card.Body>
        <h3 style={{textAlign: 'center'}}>{props.title}</h3>
        <p style={{textAlign: 'justify'}}>In hac habitasse platea dictumst. Nam malesuada eros eget vulputate gravida.
          Maecenas erat est, maximus sit amet varius eget, eleifend eget mi. Nulla non lectus arcu.</p>
      </Card.Body>
    </Card>
  )
}

export default Article
