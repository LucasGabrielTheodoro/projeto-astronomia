import * as React from "react"
import {Avatar} from "@nextui-org/react"

const ArticleAuthor = (name, image) => {
  return (
    <div style={{
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      marginBottom: '35px',
      marginTop: '10px'
    }}>
      <Avatar squared src={image}/>
      <p style={{fontWeight: 300, marginLeft: '15px', fontSize: '25px'}}>Por {name}</p>
    </div>
  )
}

export default ArticleAuthor
