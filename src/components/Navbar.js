import * as React from "react"
import {Link} from "gatsby"

const navbarDiv = {
  display: "flex",
  justifyContent: "center",
  direction: "row",
  marginBottom: '30px',
  marginTop: '20px',
  width: '100vw',
}

const navLink = {
  fontSize: '20px',
  color: '#FFFFFF',
  display: 'flex',
  marginRight: '40px'
}

const navLinkNoMargin = {
  fontSize: '20px',
  color: '#FFFFFF',
  display: 'flex',
}

const Navbar = () => {
  return (
    <div style={navbarDiv}>
      <div style={navLink}><Link style={{color: 'white'}} to={'/'}>Início</Link></div>
      <div style={navLink}><a style={{color: 'white'}} href={'https://etecitapira.com.br'}>Site Etec</a></div>
      <div style={navLinkNoMargin}><Link style={{color: 'white'}} to={'/about'}>Sobre</Link></div>
    </div>
  )
}

export default Navbar
